﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using OpenTK.Graphics;

using HT_Engine;
using HT_Engine.GameObjects;
using HT_Engine.Core;
using HT_Engine.Animations;
using HT_Engine.GameObjects.Actors;
using HT_Engine.Core.Structs;
using OpenTK;

namespace AdventureBoi
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Running Game...");
            HTWindow myWindow = new HTWindow("Adventure Boi", 800, 480, GraphicsMode.Default);

            //0.0f, 0.1f, -0.1f, -0.1f, 0.1f, -0.1f

            //TriangleParams tp = new TriangleParams(new Vector2(0.0f, 0.1f), new Vector2(-0.1f, -0.1f), new Vector2(0.1f, -0.1f));
            //TriangleObject triangle = new TriangleObject(Color.Yellow, tp);

            ActorCoords acPlayer = new ActorCoords(new Vector2[] {
                new Vector2(0.2f,-0.4f),
                new Vector2(0.4f,-0.4f),
                new Vector2(0.4f, -0.7f),
                new Vector2(0.2f, -0.7f)
            });

            ActorCoords acBkg = new ActorCoords(new Vector2[] {
                new Vector2(-1,1f),
                new Vector2(-1f,-1f),
                new Vector2(1f,-1f),
                new Vector2(1f,1f)
            });

            Actor2D bkg = new Actor2D(acBkg, Color.Azure);
            bkg.LoadActorTexture("bkg.jpg");

            Actor2D player = new Actor2D(acPlayer, Color.Yellow);
            player.LoadActorTexture("player.png");
            player.SetAsPlayer(true);
            player.makeTransparent = true;

            bool wasNewSceneLoaded = false;

            //overrrides update player function
            player.UpdateHandler = () =>
            {
                HandleMapTransition(player, myWindow, wasNewSceneLoaded);
                return 0;
            };

            Scene2D mainScene = new Scene2D();
            mainScene.AddObject(bkg);
            mainScene.AddObject(player);
            myWindow.AddScene(mainScene);

            Console.WriteLine("Count scenes:" + myWindow.scenes.Count);
            Console.WriteLine("Scene objects:" + myWindow.scenes[myWindow.scenes.Count - 1].GameObjects.Count);

            myWindow.SetCurrentScene(myWindow.scenes.Count - 1);
            myWindow.Run();
        }

        private static void HandleMapTransition(Actor2D player, HTWindow win, bool wasLoaded)
        {
            //Console.WriteLine("//////////////////////////////////////" + player.coords + "///////////////////////");
            if (player.coords.pts[0].X <= -1.2f && (player.coords.pts[0].Y >= 0.4f))
            {
                if (wasLoaded == false)
                {
                    ActorCoords acBkg2 = new ActorCoords(new Vector2[] {
                        new Vector2(-1,1f),
                        new Vector2(-1f,-1f),
                        new Vector2(1f,-1f),
                        new Vector2(1f,1f)
                    });

                    Actor2D bkg2 = new Actor2D(acBkg2, Color.Azure);
                    bkg2.LoadActorTexture("bkg2.png");

                    win.scenes.Clear();

                    ActorCoords acPlayer = new ActorCoords(new Vector2[] {
                        new Vector2(0.8f,0.5f),
                        new Vector2(1f,0.5f),
                        new Vector2(1f, 0.2f),
                        new Vector2(0.8f, 0.2f)
                     });

                    ActorCoords acEnemy = new ActorCoords(new Vector2[] {
                        new Vector2(0,0),
                        new Vector2(0.2f,0),
                        new Vector2(0.2f, -0.3f),
                        new Vector2(0, -0.3f)
                     });

                    Actor2D enemy1 = new Actor2D(acEnemy, Color.Yellow);
                    enemy1.LoadActorTexture("enemy1.png");
                    enemy1.makeTransparent = true;

                    ActorCoords acEnemy2 = new ActorCoords(new Vector2[] {
                        new Vector2(-0.4f,0),
                        new Vector2(-0.2f,0),
                        new Vector2(-0.2f, -0.3f),
                        new Vector2(-0.4f, -0.3f)
                     });

                    Actor2D enemy2 = new Actor2D(acEnemy2, Color.Yellow);
                    enemy2.LoadActorTexture("enemy1.png");
                    enemy2.makeTransparent = true;

                    Actor2D player2 = new Actor2D(acPlayer, Color.Yellow);
                    player2.LoadActorTexture("player.png");
                    player2.SetAsPlayer(true);
                    player2.makeTransparent = true;

                    Scene2D mainScene = new Scene2D();
                    mainScene.AddObject(bkg2);
                    mainScene.AddObject(player2);
                    mainScene.AddObject(enemy1);
                    mainScene.AddObject(enemy2);

                    enemy1.UpdateHandler = () =>
                    {
                        EnemyHandlerP1(mainScene, enemy1, player2);
                        return 0;
                    };

                    enemy2.UpdateHandler = () =>
                    {
                        EnemyHandlerP2(mainScene, enemy2, player2);
                        return 0;
                    };

                    win.AddScene(mainScene);
                }
            }
        }

        private static int moveUpP1 = 1;
        private static void EnemyHandlerP1(Scene2D scene, Actor2D e, Actor2D player)
        {
            if (moveUpP1 == 1)
            {
                e.Move(new Vector2(0, 0.01f));
                if (e.coords.pts[0].Y >= 0.8f)
                {
                    moveUpP1 = 2;
                }
            }
            else if (moveUpP1 == 2)
            {
                if (e.coords.pts[3].Y <= -0.8f)
                {
                    moveUpP1 = 1;
                }
                e.Move(new Vector2(0, -0.01f));
            }

            if ((player.coords.pts[0].X < e.coords.pts[1].X) && (player.coords.pts[0].X > e.coords.pts[0].X))
            {
                if (player.coords.pts[0].Y <= e.coords.pts[2].Y || player.coords.pts[0].Y >= e.coords.pts[1].Y)
                {
                    return;
                }
                e.LoadActorTexture("deadEnemy.png");
                moveUpP1 = 3;
            }
        }

        private static int moveUpP2 = 1;
        private static void EnemyHandlerP2(Scene2D scene, Actor2D e, Actor2D player)
        {
            if (moveUpP2 == 1)
            {
                e.Move(new Vector2(0, 0.01f));
                if (e.coords.pts[0].Y >= 0.8f)
                {
                    moveUpP2 = 2;
                }
            }
            else if (moveUpP2 == 2)
            {
                if (e.coords.pts[3].Y <= -0.8f)
                {
                    moveUpP2 = 1;
                }
                e.Move(new Vector2(0, -0.01f));
            }

            if ((player.coords.pts[0].X < e.coords.pts[1].X) && (player.coords.pts[0].X > e.coords.pts[0].X))
            {
                if (player.coords.pts[0].Y <= e.coords.pts[2].Y || player.coords.pts[0].Y >= e.coords.pts[1].Y)
                {
                    return;
                }
                e.LoadActorTexture("deadEnemy.png");
                moveUpP2 = 3;
            }
        }
    }
}


//if (args.Key == Key.W)
//{
//    scenes[CurrentScene].GameObjects[1].Move(new Vector2(0, speed));
//}

//if (args.Key == Key.S)
//{
//    scenes[CurrentScene].GameObjects[1].Move(new Vector2(0, y: -speed));
//}