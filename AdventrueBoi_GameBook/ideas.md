Postacie:
    Ogólne:
        1 gracz
        1 Handlarz 
        1 strażniczka wioski (Tidra Idri) - daje misje
        3 randomów którzy robią za tłum 
        5 rodzaji przeciwników 
        1 BOSS

    Techniczne:
        Interfesy dla przeciwników( pole level, obrażenia, atack speed, zdrowie)

Mechaniki:
    Ogólne:
        gracz:
            Zwiększenie prędkości ruchu
            Potęzny cios
            Blokowanie obrażeń
        Przeciwnicy:
            Kula ognia
            Doskok
            Spowolnienie
            Trucizna

    Techniczne:
        Interfejs czarów
        Potężny cios 2x dmg

Logika:
    Ogólne: 
        - Po śmierci gracz odradza się w mieście
        - Potwory dropią przedmioty
        - Skrzynie w labiryntach
        - Pobieranie misji
        - Drogowskazy jako teleporty/portale do lokacji

    Techinczy:
        - każdy potwór ma liste rzeczy które może wydropić

UX/UI:
    Ogólne:
        - Menu ekwipunku
        - menu pauzy / zapisu
        - Zakupy
        - menu główne
        - ncs music:
            - https://www.youtube.com/watch?v=7_cwKd81z7Q
        - interfejs gracza:
            - pasek życia
            - złoto
            - pasek energii
        - wyświetlanie ilości złota nad graczem po zabiciu przeciwnika
        - ekran śmierci
        - dźwięki:
            - śmierć
            - walka
            - atak
            - umiejka
            - zakup
    
    Techniczne:
        - Wygaszanie ekranu przy zmianie lokacji

Silnik:
    - klasa które pozwalają ładować rózne perspektywy gracza
        -> klasa animacja